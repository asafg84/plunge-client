var hbs = require('handlebars');
module.exports = function () {
    var _helpers = {};
    
    console.log('registers _helpers');

    _helpers.test = function() {
        return 'test';
    };

    _helpers.isEqual = function(v1, v2, options) {
        if(v1 === v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    };
    
    _helpers.filename = function(s) {
        var escaped = escape(s);
//        console.log(escaped);

        return escaped;
    };
    
    return _helpers;
}