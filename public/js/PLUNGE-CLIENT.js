var PLUNGE = {
    init: function(videotag, on_success) {
        if (this.EXISTS(videotag)) {
            videotag.onpause = function() {
                PLUNGE.events.onpause(this);
            };
            videotag.onplaying = function() {
                PLUNGE.events.onplaying(this);
            };
            videotag.onplay = function() {
                PLUNGE.events.currentTime(this);
            };
            videotag.onended = function() {
                PLUNGE.events.ended(this);
            };
            videotag.onseeked = function() {
                PLUNGE.events.onseeked(this);
            };
            videotag.ontimeupdate = function() {
                PLUNGE.events.currentTime(this);
            };
            videotag.oncanplay = function() {
                PLUNGE.events.oncanplay(this);
            };
            videotag.onloadstart = function() {
                PLUNGE.events.onloadstart(this);
            };
        }
        this.events.initClient(on_success);
    },
    events: {
        //client route
        initClient: function(on_success) {
            if (PLUNGE.EXISTS(on_success)) {
                PLUNGE.AJAX('/client/initClient', 'POST', {action:'init-client'}, on_success, PLUNGE.ON_ERROR);
                return;
            }
            PLUNGE.AJAX('/client/initClient', 'POST', {action:'init-client'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        stopClient: function() {
            PLUNGE.AJAX('/client/stopClient', 'POST', {action:'stop-client'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        //player route
        onpause: function() {
            PLUNGE.AJAX('/player/onpause', 'POST', {action:'on-pause'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        onplaying: function(videotag) {
            PLUNGE.AJAX('/player/onplaying?currentTime=' + videotag.currentTime, 'POST', {currentTime:videotag.currentTime, action:'onplaying'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        currentTime: function(videotag) {
            PLUNGE.AJAX('/player/currentTime?currentTime=' + videotag.currentTime, 'POST', {currentTime:videotag.currentTime, action:'currentTime'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        ended: function(videotag) {
            PLUNGE.AJAX('/player/ended', 'POST', {ended:true, action:'ended'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        onseeked: function(videotag) {
            PLUNGE.AJAX('/player/onseeked=' + videotag.currentTime, 'POST', {currentTime:videotag.currentTime, action:'onseeked'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        oncanplay: function(videotag) {
            PLUNGE.AJAX('/player/oncanplay', 'POST', {oncanplay:true, action:'oncanplay'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        onloadstart: function(videotag) {
            PLUNGE.AJAX('http://localhost:3000/subtitles?filename=8bit_transform.mp4/player/onloadstart', 'GET', {}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        },
        uploadscript: function(params, on_success) {
            this.fire({method:'colorRgbHex',selector:'all',hex:'#000000',duration: 0});
            if (PLUNGE.EXISTS(on_success)) {
                PLUNGE.AJAX('/player/uploadscript', 'POST', {script:params.script, filename:params.scriptname, action:'uploadscript'}, on_success, PLUNGE.ON_ERROR);
            } else {
                PLUNGE.AJAX('/player/uploadscript', 'POST', {script:params.script, filename:params.scriptname, action:'uploadscript'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
            }
        },
        unloadscript: function(on_success) {
            this.fire({method:'colorRgbHex',selector:'all',hex:'#000000',duration: 0});
            if (PLUNGE.EXISTS(on_success)) {
                PLUNGE.AJAX('/player/unloadScript', 'POST', {action:'unloadScript'}, on_success, PLUNGE.ON_ERROR);
            } else {
                PLUNGE.AJAX('/player/unloadScript', 'POST', {action:'unloadScript'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
            }
        },
        fire: function(params) {
            PLUNGE.AJAX('/player/fire', 'POST', {params:params,action:'fire'}, PLUNGE.ON_SUCCESS, PLUNGE.ON_ERROR);
        }
    },
    EXISTS: function(obj) {
        return (obj && obj !== 'null' && obj !== 'undefined');
    },
    AJAX: function(url, method, _data, onSuccess, onError) {
        $.ajax({
            url     : url,
            type    : method,
            data    : _data,
            success : onSuccess,
            error   : onError
        })
    },
    ON_SUCCESS: function(res, textStatus, jqXHR) {
        try {
            if (PLUNGE.EXISTS(res)) {
                return console.log(res);
            } else if (PLUNGE.EXISTS(textStatus)) {
                return console.log(textStatus);
            } else if (PLUNGE.EXISTS(jqXHR)) {
                return console.log(jqXHR);
            } 
            console.log('PLUNGE ON SUCCESS');
        } catch(e) {
            console.log(e);
        }
    },
    ON_ERROR: function(jqXHR, textStatus, errorThrown) {
        try {
            if (PLUNGE.EXISTS(jqXHR)) {
                return console.log(jqXHR);
            } else if (PLUNGE.EXISTS(textStatus)) {
                return console.log(textStatus);
            } else if (PLUNGE.EXISTS(errorThrown)) {
                return console.log(errorThrown);
            } 
            console.log('internal error');
        } catch(e) {
            console.log(e);
        }
    }
}