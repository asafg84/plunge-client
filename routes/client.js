var express     = require('express'),
    app         = express(),
    router      = express.Router();

router.post('/initClient', function (req, res) {
    res.status(200).send(req.body.action);
});

router.post('/stopClient', function (req, res) {
    res.status(200).send(req.body.action);
});

module.exports = router;