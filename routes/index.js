var express     = require('express'),
    middleware  = require('./middleware'),
    app         = express(),
    router      = express.Router();

router.get('/', middleware.initLocals, function (req, res) {
    res.render('index');
});

module.exports = router;