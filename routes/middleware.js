var mediafoldername = require('path').resolve(__dirname, '..', 'public', 'media');
var fs              = require('fs');

exports.initLocals = function (req, res, next) {
    fs.readdir(mediafoldername, (err, files) => {
        if (err) {
            console.log(err);
        } else {
            res.locals.media = files;
            files.forEach(file => {
            });
        }
        
        if (req.headers.host.indexOf('localhost') > -1) {
            res.locals.host = 'localhost:3000';
        } else {
            res.locals.host = conf.remote.ip;
        }
        
        next();
    });
}