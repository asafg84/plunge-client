var express             = require('express'),
    path                = require('path'),
    app                 = express(),
    bodyParser          = require('body-parser'),
    morgan              = require('morgan');

//view
var handlebars = require('express-handlebars').create({
    layoutsDir: path.join(__dirname, "views/layouts"),
    partialsDir: path.join(__dirname, "views/partials"),
    defaultLayout: 'main',
    extname: 'hbs',
    helpers: new require('./views/helpers')()
});

app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
app.use(morgan('dev'));

app.engine('hbs', handlebars.engine);
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, "views"));

app.use(express.static(__dirname + '/public'));

var port = process.env.PORT || 3000;

if (require.main === module) {
    var root = require('./routes/index');
    app.use('/', root);
    var client = require('./routes/client');
    app.use('/client', client);
    var player = require('./routes/player');
    app.use('/player', player);
}

if (require.main === module) {
    app.listen(port);  
    console.log('We\'ve got ourselves a convoy on port ' + port); 
}

module.exports = app;